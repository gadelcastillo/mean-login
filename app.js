'use strict'

const express = require('express');
const api = require('./routes');
const app = express();

// Middleware
const auth = require('./middleware/auth')
// Models 

// Controllers 


app.use(express.urlencoded({ extended: false}))
app.use(express.json());
app.use('/', api);

app.use(express.static(__dirname + '/dist/noGain-app'));


module.exports = app;