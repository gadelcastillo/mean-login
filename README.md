# mean-login

Metodo de registro de clientes nuevos, los cuales no pueden registrarse multiples veces. 

Existe un panel administrativo para el staff el cual administra ciudades y sedes a las cuales se registraran los clientes. Cada sede tiene un limite de 300 clientes.

El staff tiene la oportunidad de visualizar los clientes registrados por sede. 

### Getting Started 

## Requerimientos tecnicos

Debido a que la api esta desarrollada en javascript debe instalar Node.js y npm en su version LTS. De no tener puede dercargarlo en el siguiente link :

* [Node.js](https://nodejs.org/es/)

El motor de base de datos que utilizar la api es MongoDB el cual se puede descargar desde la pagina:

* [MongoDB](https://www.mongodb.com/what-is-mongodb)


## Implementación

1. Clonar el repositorio
    git clone https://gitlab.com/gadelcastillo/mean-login.git

2. Ingresar al repositorio 
    ```
    cd mean-login
    ```

3. Instalar todas las dependencias 
    ```
    npm install
    ```

4. Iniciar el proyecto
    ```
    npm start
    ```

**Nota:** El proyecto correra en el la direccion http://localhost:3000/

El usuario inicial sera creado automaticamente al inicial la aplicacion
Puede ser creado desde el request "Crear usuario" en postman

Usuario administrador: Admin@gmail.com
Contraseña: 123123123

## Autor
Gustavo Del Castillo 

## License

Este proyecto se encuentra bajo licencia GNU GPL v3 - ver el archivo [LICENSE.md](LICENSE.md) para más detalle 

## Postman Collection 

https://www.getpostman.com/collections/8589066c64564a06f40c