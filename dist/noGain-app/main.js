(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_registration_registration_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/registration/registration.component */ "./src/app/pages/registration/registration.component.ts");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/login/login.component */ "./src/app/pages/login/login.component.ts");
/* harmony import */ var _pages_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/navbar/navbar.component */ "./src/app/pages/navbar/navbar.component.ts");
/* harmony import */ var _pages_client_client_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/client/client.component */ "./src/app/pages/client/client.component.ts");
/* harmony import */ var _pages_city_city_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/city/city.component */ "./src/app/pages/city/city.component.ts");
/* harmony import */ var _pages_boffice_boffice_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/boffice/boffice.component */ "./src/app/pages/boffice/boffice.component.ts");
/* harmony import */ var _guards_islogged_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./guards/islogged.guard */ "./src/app/guards/islogged.guard.ts");










var routes = [
    { path: '', redirectTo: 'register', pathMatch: 'full' },
    { path: 'register', component: _pages_registration_registration_component__WEBPACK_IMPORTED_MODULE_3__["RegistrationComponent"] },
    { path: 'login', component: _pages_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'dashboard', component: _pages_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"], canActivate: [_guards_islogged_guard__WEBPACK_IMPORTED_MODULE_9__["IsloggedGuard"]],
        children: [
            { path: 'cities', component: _pages_city_city_component__WEBPACK_IMPORTED_MODULE_7__["CityComponent"] },
            { path: 'branches', component: _pages_boffice_boffice_component__WEBPACK_IMPORTED_MODULE_8__["BofficeComponent"] },
            { path: 'clients', component: _pages_client_client_component__WEBPACK_IMPORTED_MODULE_6__["ClientComponent"] }
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'noGain-app';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./material/material.module */ "./src/app/material/material.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _pages_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/navbar/navbar.component */ "./src/app/pages/navbar/navbar.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _pages_registration_registration_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/registration/registration.component */ "./src/app/pages/registration/registration.component.ts");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/login/login.component */ "./src/app/pages/login/login.component.ts");
/* harmony import */ var _services_boffice_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/boffice.service */ "./src/app/services/boffice.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _pages_client_client_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/client/client.component */ "./src/app/pages/client/client.component.ts");
/* harmony import */ var _pages_city_city_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/city/city.component */ "./src/app/pages/city/city.component.ts");
/* harmony import */ var _pages_boffice_boffice_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/boffice/boffice.component */ "./src/app/pages/boffice/boffice.component.ts");
/* harmony import */ var _guards_islogged_guard__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./guards/islogged.guard */ "./src/app/guards/islogged.guard.ts");



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _pages_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                _pages_registration_registration_component__WEBPACK_IMPORTED_MODULE_11__["RegistrationComponent"],
                _pages_login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _pages_client_client_component__WEBPACK_IMPORTED_MODULE_15__["ClientComponent"],
                _pages_city_city_component__WEBPACK_IMPORTED_MODULE_16__["CityComponent"],
                _pages_boffice_boffice_component__WEBPACK_IMPORTED_MODULE_17__["BofficeComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _material_material_module__WEBPACK_IMPORTED_MODULE_3__["MaterialModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"]
            ],
            providers: [_services_boffice_service__WEBPACK_IMPORTED_MODULE_13__["BofficeService"], _guards_islogged_guard__WEBPACK_IMPORTED_MODULE_18__["IsloggedGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/guards/islogged.guard.ts":
/*!******************************************!*\
  !*** ./src/app/guards/islogged.guard.ts ***!
  \******************************************/
/*! exports provided: IsloggedGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsloggedGuard", function() { return IsloggedGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");




var IsloggedGuard = /** @class */ (function () {
    function IsloggedGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    IsloggedGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.auth.isAuth()) {
                console.log('auth');
                resolve(true);
            }
            else {
                _this.router.navigate(['/login']);
                resolve(false);
            }
        });
    };
    IsloggedGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], IsloggedGuard);
    return IsloggedGuard;
}());



/***/ }),

/***/ "./src/app/material/material.module.ts":
/*!*********************************************!*\
  !*** ./src/app/material/material.module.ts ***!
  \*********************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm5/a11y.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm5/portal.es5.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/esm5/stepper.es5.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm5/tree.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");











var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [
                _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_3__["A11yModule"],
                _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_7__["CdkStepperModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_8__["CdkTableModule"],
                _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_9__["CdkTreeModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__["DragDropModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTreeModule"],
                _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__["PortalModule"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_6__["ScrollingModule"],
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/pages/boffice/boffice.component.css":
/*!*****************************************************!*\
  !*** ./src/app/pages/boffice/boffice.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYm9mZmljZS9ib2ZmaWNlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYm9mZmljZS9ib2ZmaWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/boffice/boffice.component.html":
/*!******************************************************!*\
  !*** ./src/app/pages/boffice/boffice.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <h3 class=\"mt-3\">Oficinas</h3>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-1\"></div>\n    <div class=\" col-xs-12 col-sm-12 col-md-10\">\n      <mat-card>\n        <mat-card-header>\n          <mat-card-title>\n            <button (click)=\"openDialog(Modal)\" mat-raised-button color=\"primary\">\n              Nueva Oficina\n            </button>\n          </mat-card-title>\n        </mat-card-header>\n        <mat-divider></mat-divider>\n        <mat-card-content>\n          <div class=\"row mt-2\">\n            <div class=\"col-md-12\" align=\"right\">\n              <mat-form-field>\n                <mat-select selected=\"all\" (selectionChange)=\"getOfficesbyId($event.value)\" placeholder=\"Filtrar por ciudad\">\n                  <mat-option value=\"all\" >Todas</mat-option>\n                  <mat-option *ngFor=\"let city of cities\" value=\"{{city._id}}\">{{city.name}}</mat-option>\n                </mat-select>\n              </mat-form-field>\n            </div>\n          </div>\n          <div class=\"row  mt-3\">\n            <div class=\"col-md-12 col-xs-12\">\n              <table mat-table class=\"mat-elevation-z2\" [dataSource]=\"dataSource\" matSort>\n                <ng-container matColumnDef=\"numero\">\n                  <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>\n                  <td mat-cell *matCellDef=\"let row\">\n                    {{ dataSource.filteredData.indexOf(row) + 1 }}\n                  </td>\n                </ng-container>\n                <ng-container matColumnDef=\"nombre\">\n                  <th mat-header-cell *matHeaderCellDef>Nombre</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.name }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"clientes\">\n                  <th mat-header-cell *matHeaderCellDef>Clientes</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.clients }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"registro\">\n                  <th mat-header-cell *matHeaderCellDef>Fecha de registro</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.registered }}</td>\n                </ng-container>\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\n              </table>\n              <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\">\n              </mat-paginator>\n            </div>\n          </div>\n        </mat-card-content>\n      </mat-card>\n    </div>\n    <div class=\"col-md-1\"></div>\n  </div>\n</div>\n\n<ng-template #Modal>\n  <h3>Agregar Sucursal</h3>\n  <form [formGroup]=\"createForm\">\n    <div class=\"example-container\">\n      <mat-form-field>\n        <input formControlName=\"name\" matInput placeholder=\"Nombre de la sucursal\">\n        <mat-error *ngIf=\"G.name.errors && submitted\">Campo requerido</mat-error>\n      </mat-form-field>\n      <mat-form-field>\n        <mat-select formControlName=\"city\" placeholder=\"Seleccione la ciudad\">\n          <mat-option *ngFor=\"let city of cities\" value=\"{{city._id}}\">{{city.name}}</mat-option>\n        </mat-select>\n        <mat-error *ngIf=\"G.city.errors && submitted\">Campo requerido</mat-error>\n      </mat-form-field>\n    </div>\n  </form>\n  <mat-divider></mat-divider>\n  <div class=\"mt-1\" align=\"right\">\n    <button align=\"right\" color=\"accent\" mat-raised-button (click)=\"submitForm()\">Agregar</button>\n  </div>\n</ng-template>\n\n"

/***/ }),

/***/ "./src/app/pages/boffice/boffice.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/boffice/boffice.component.ts ***!
  \****************************************************/
/*! exports provided: BofficeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BofficeComponent", function() { return BofficeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_boffice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/boffice.service */ "./src/app/services/boffice.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var BofficeComponent = /** @class */ (function () {
    function BofficeComponent(boffice, dialog, snackbar, formbuilder) {
        var _this = this;
        this.boffice = boffice;
        this.dialog = dialog;
        this.snackbar = snackbar;
        this.formbuilder = formbuilder;
        this.displayedColumns = ['numero', 'nombre', 'clientes', 'registro'];
        this.submitted = false;
        this.getBranches = function () {
            _this.boffice.getBranches().subscribe(function (data) {
                // console.log(data['branches']);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data['branches']);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
            });
        };
        this.getOfficesbyId = function (id) {
            if (id === 'all') {
                _this.getBranches();
            }
            else {
                _this.boffice.getOfficesbyId(id).subscribe(function (data) {
                    _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data['branches']);
                    _this.dataSource.paginator = _this.paginator;
                    _this.dataSource.sort = _this.sort;
                });
            }
        };
        this.getCities = function () {
            _this.boffice.getCities().subscribe(function (data) {
                _this.cities = data['cities'];
            });
        };
        this.submitForm = function () {
            _this.submitted = true;
            if (_this.createForm.invalid) {
                return;
            }
            _this.boffice.postBranch(_this.createForm.value).subscribe(function (response) {
                if (response['result'] !== 'Error') {
                    _this.snackbar.open(response['message'], response['result'], { duration: 3000 });
                    _this.dialogRef.close();
                    _this.createForm.reset();
                    _this.getBranches();
                }
            });
        };
    }
    BofficeComponent.prototype.ngOnInit = function () {
        this.getBranches();
        this.getCities();
        this.createForm = this.formbuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    };
    Object.defineProperty(BofficeComponent.prototype, "G", {
        get: function () { return this.createForm.controls; },
        enumerable: true,
        configurable: true
    });
    BofficeComponent.prototype.openDialog = function (templateId) {
        this.dialogRef = this.dialog.open(templateId, {
            width: '60%',
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], BofficeComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], BofficeComponent.prototype, "sort", void 0);
    BofficeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-boffice',
            template: __webpack_require__(/*! ./boffice.component.html */ "./src/app/pages/boffice/boffice.component.html"),
            styles: [__webpack_require__(/*! ./boffice.component.css */ "./src/app/pages/boffice/boffice.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_boffice_service__WEBPACK_IMPORTED_MODULE_2__["BofficeService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], BofficeComponent);
    return BofficeComponent;
}());



/***/ }),

/***/ "./src/app/pages/city/city.component.css":
/*!***********************************************!*\
  !*** ./src/app/pages/city/city.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2l0eS9jaXR5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY2l0eS9jaXR5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/city/city.component.html":
/*!************************************************!*\
  !*** ./src/app/pages/city/city.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <h3 class=\"mt-3\">Ciudades</h3>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-1\"></div>\n    <div class=\" col-xs-12 col-sm-12 col-md-10\">\n      <mat-card>\n        <mat-card-header>\n          <mat-card-title>\n            <button\n              (click)=\"openDialog(Modal)\"\n              mat-raised-button\n              color=\"primary\"\n            >\n              Nueva Ciudad\n            </button>\n          </mat-card-title>\n        </mat-card-header>\n        <mat-divider></mat-divider>\n        <mat-card-content>\n          <div class=\"row  mt-3\">\n            <div class=\"col-md-12 col-xs-12\">\n              <table mat-table class=\"mat-elevation-z2\" [dataSource]=\"dataSource\" matSort>\n                <ng-container matColumnDef=\"numero\">\n                  <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>\n                  <td mat-cell *matCellDef=\"let row\">\n                    {{ dataSource.filteredData.indexOf(row) + 1 }}\n                  </td>\n                </ng-container>\n                <ng-container matColumnDef=\"ciudad\">\n                  <th mat-header-cell *matHeaderCellDef>Ciudad</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.name }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"estado\">\n                  <th mat-header-cell *matHeaderCellDef>Departamento</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.state }}</td>\n                </ng-container>\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\n              </table>\n              <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\">\n              </mat-paginator>\n            </div>\n          </div>\n        </mat-card-content>\n      </mat-card>\n    </div>\n    <div class=\"col-md-1\"></div>\n  </div>\n</div>\n\n<ng-template #Modal>\n  <h3>Agregar Ciudad</h3>\n  <form [formGroup]=\"createForm\">\n    <div class=\"example-container\">\n      <mat-form-field>\n        <input formControlName=\"city_name\" matInput placeholder=\"Nombre de la ciudad\">\n        <mat-error *ngIf=\"G.city_name.errors && submitted\">Campo requerido</mat-error>\n      </mat-form-field>\n      <mat-form-field>\n        <input formControlName=\"state\" type=\"text\" matInput placeholder=\"Contraseña\"/>\n        <mat-error *ngIf=\"G.state.errors && submitted\">Campo requerido</mat-error>\n      </mat-form-field>\n    </div>\n  </form>\n  <mat-divider></mat-divider>\n  <div class=\"mt-1\" align=\"right\">\n    <button align=\"right\" color=\"accent\" mat-button (click)=\"submitForm()\">Agregar</button>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/pages/city/city.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/city/city.component.ts ***!
  \**********************************************/
/*! exports provided: CityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CityComponent", function() { return CityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_boffice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/boffice.service */ "./src/app/services/boffice.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var CityComponent = /** @class */ (function () {
    function CityComponent(boffice, dialog, snackbar, formbuilder) {
        var _this = this;
        this.boffice = boffice;
        this.dialog = dialog;
        this.snackbar = snackbar;
        this.formbuilder = formbuilder;
        this.displayedColumns = ['numero', 'ciudad', 'estado'];
        this.submitted = false;
        this.getCities = function () {
            _this.boffice.getCities().subscribe(function (data) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data['cities']);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
            });
        };
        this.submitForm = function () {
            _this.submitted = true;
            if (_this.createForm.invalid) {
                return;
            }
            _this.boffice.postCity(_this.createForm.value).subscribe(function (response) {
                if (response['result'] !== 'Error') {
                    _this.snackbar.open(response['message'], response['result'], { duration: 3000 });
                    _this.dialogRef.close();
                    _this.createForm.reset();
                    _this.getCities();
                }
            });
        };
    }
    CityComponent.prototype.ngOnInit = function () {
        this.getCities();
        this.createForm = this.formbuilder.group({
            city_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    };
    CityComponent.prototype.openDialog = function (templateId) {
        this.dialogRef = this.dialog.open(templateId, {
            width: '60%',
        });
    };
    Object.defineProperty(CityComponent.prototype, "G", {
        get: function () { return this.createForm.controls; },
        enumerable: true,
        configurable: true
    });
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], CityComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], CityComponent.prototype, "sort", void 0);
    CityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-city',
            template: __webpack_require__(/*! ./city.component.html */ "./src/app/pages/city/city.component.html"),
            styles: [__webpack_require__(/*! ./city.component.css */ "./src/app/pages/city/city.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_boffice_service__WEBPACK_IMPORTED_MODULE_2__["BofficeService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], CityComponent);
    return CityComponent;
}());



/***/ }),

/***/ "./src/app/pages/client/client.component.css":
/*!***************************************************!*\
  !*** ./src/app/pages/client/client.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xpZW50L2NsaWVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NsaWVudC9jbGllbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/client/client.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/client/client.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <h3 class=\"mt-3\">Clientes</h3>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-1\"></div>\n    <div class=\" col-xs-12 col-sm-12 col-md-10\">\n      <mat-card>\n        <mat-card-header>\n          <mat-card-title>\n            <button  mat-raised-button color=\"primary\">\n              Clientes\n            </button>\n          </mat-card-title>\n        </mat-card-header>\n        <mat-divider></mat-divider>\n        <mat-card-content>\n          <div class=\"row mt-2\">\n            <div class=\"col-md-12\" align=\"right\">\n              <mat-form-field>\n                <mat-select selected=\"all\" (selectionChange)=\"getClientsbyOffice($event.value)\" placeholder=\"Filtrar por Sucursal\">\n                  <mat-option value=\"all\" >Todas</mat-option>\n                  <mat-option *ngFor=\"let branch of branches\" value=\"{{branch._id}}\">{{branch.name}}</mat-option>\n                </mat-select>\n              </mat-form-field>\n            </div>\n          </div>\n          <div class=\"row  mt-3\">\n            <div class=\"col-md-12 col-xs-12\">\n              <table mat-table class=\"mat-elevation-z2\" [dataSource]=\"dataSource\" matSort>\n                <ng-container matColumnDef=\"numero\">\n                  <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>\n                  <td mat-cell *matCellDef=\"let row\">\n                    {{ dataSource.filteredData.indexOf(row) + 1 }}\n                  </td>\n                </ng-container>\n                <ng-container matColumnDef=\"nombre\">\n                  <th mat-header-cell *matHeaderCellDef>Nombre</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.name }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"dni\">\n                  <th mat-header-cell *matHeaderCellDef>Cedula</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.dni }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"correo\">\n                  <th mat-header-cell *matHeaderCellDef>Correo</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.email }}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"registro\">\n                  <th mat-header-cell *matHeaderCellDef>Fecha de registro</th>\n                  <td mat-cell *matCellDef=\"let row\">{{ row.registered }}</td>\n                </ng-container>\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\n              </table>\n              <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\">\n              </mat-paginator>\n            </div>\n          </div>\n        </mat-card-content>\n      </mat-card>\n    </div>\n    <div class=\"col-md-1\"></div>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/pages/client/client.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/client/client.component.ts ***!
  \**************************************************/
/*! exports provided: ClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientComponent", function() { return ClientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_boffice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/boffice.service */ "./src/app/services/boffice.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");





var ClientComponent = /** @class */ (function () {
    function ClientComponent(boffice, clientService) {
        var _this = this;
        this.boffice = boffice;
        this.clientService = clientService;
        this.displayedColumns = ['numero', 'nombre', 'dni', 'correo', 'registro'];
        this.getBranches = function () {
            _this.boffice.getBranches().subscribe(function (data) {
                // console.log(data['branches']);
                _this.branches = data['branches'];
            });
        };
        this.getClients = function () {
            _this.clientService.getClients().subscribe(function (data) {
                console.log(data['clients']);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data['clients']);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
            });
        };
        this.getClientsbyOffice = function (id) {
            if (id === 'all') {
                _this.getClients();
            }
            else {
                _this.clientService.getClientbyOffice(id).subscribe(function (data) {
                    console.log(data['clients']);
                    _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data['clients']);
                    _this.dataSource.paginator = _this.paginator;
                    _this.dataSource.sort = _this.sort;
                });
            }
        };
    }
    ClientComponent.prototype.ngOnInit = function () {
        this.getBranches();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ClientComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ClientComponent.prototype, "sort", void 0);
    ClientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-client',
            template: __webpack_require__(/*! ./client.component.html */ "./src/app/pages/client/client.component.html"),
            styles: [__webpack_require__(/*! ./client.component.css */ "./src/app/pages/client/client.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_boffice_service__WEBPACK_IMPORTED_MODULE_2__["BofficeService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], ClientComponent);
    return ClientComponent;
}());



/***/ }),

/***/ "./src/app/pages/login/login.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/login/login.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body { \n    background-color: lightgrey;\n}\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n\n.mt {\n    margin-top: 80px;\n}\n\nbutton {\n    margin-right: 50px;\n}\n\n.example-spacer {\n  flex: 1 1 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDJCQUEyQjtBQUMvQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHkgeyBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm10IHtcbiAgICBtYXJnaW4tdG9wOiA4MHB4O1xufVxuXG5idXR0b24ge1xuICAgIG1hcmdpbi1yaWdodDogNTBweDtcbn1cblxuLmV4YW1wbGUtc3BhY2VyIHtcbiAgZmxleDogMSAxIGF1dG87XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/login/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/login/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <span>No Pain No Gain</span>\n  <span class=\"example-spacer\"></span>\n  <button [routerLink]=\"[ '/register' ]\" mat-button>Registro</button>\n</mat-toolbar>\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-2\"></div>\n    <div class=\"col-md-8 col-xs-12 col-sm-12 mt\">\n      <mat-card class=\"example-card\">\n        <mat-card-header>\n          <mat-card-title>Iniciar Sesion!</mat-card-title>\n          <mat-card-subtitle>Ingresar al sistema!</mat-card-subtitle>\n        </mat-card-header>\n        <mat-card-content>\n          <form [formGroup]=\"loginForm\">\n            <div class=\"example-container\">\n              <mat-form-field>\n                <input formControlName=\"username\" matInput placeholder=\"Correo Electronico\" />\n                <mat-error *ngIf=\"G.username.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n              <mat-form-field>\n                <input formControlName=\"password\" type=\"password\" matInput placeholder=\"Contraseña\" />\n                <mat-error *ngIf=\"G.password.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n            </div>\n          </form>\n        </mat-card-content>\n        <mat-card-actions>\n          <div class=\"container\" align=\"right\">\n            <button color=\"accent\" (click)=\"signIn()\" mat-raised-button>Iniciar Sesión</button>\n          </div>\n        </mat-card-actions>\n      </mat-card>\n    </div>\n    <div class=\"col-md-2\"></div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_localstorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/localstorage.service */ "./src/app/services/localstorage.service.ts");







var LoginComponent = /** @class */ (function () {
    function LoginComponent(builder, auth, snackbar, router, store) {
        var _this = this;
        this.builder = builder;
        this.auth = auth;
        this.snackbar = snackbar;
        this.router = router;
        this.store = store;
        this.submitted = false;
        this.signIn = function () {
            _this.submitted = true;
            if (_this.loginForm.invalid) {
                return;
            }
            console.log(_this.loginForm.value);
            _this.auth.authenticate(_this.loginForm.value).subscribe(function (data) {
                _this.store.setItem('auth', data['token']);
                _this.snackbar.open(data['message'], data['result'], { duration: 3000 })
                    .afterDismissed().subscribe(function (action) {
                    _this.router.navigate([data['redirectTo']]);
                });
            });
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.builder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "G", {
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/pages/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/pages/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_localstorage_service__WEBPACK_IMPORTED_MODULE_6__["LocalstorageService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/pages/navbar/navbar.component.css":
/*!***************************************************!*\
  !*** ./src/app/pages/navbar/navbar.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usd0JBQWdCO0VBQWhCLGdCQUFnQjtFQUNoQixNQUFNO0VBQ04sVUFBVTtBQUNaIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdiB7XG4gIHdpZHRoOiAyMDBweDtcbn1cblxuLnNpZGVuYXYgLm1hdC10b29sYmFyIHtcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1hdC10b29sYmFyLm1hdC1wcmltYXJ5IHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICB6LWluZGV4OiAxO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/navbar/navbar.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/navbar/navbar.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"true\"\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n      [opened]=\"!(isHandset$ | async)\">\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n      <a mat-list-item [routerLink]=\"[ 'clients' ]\">Clientes</a>\n      <a mat-list-item [routerLink]=\"[ 'branches' ]\">Sucursales</a>\n      <a mat-list-item [routerLink]=\"[ 'cities' ]\">Ciudades</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\"\n        *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n      <span>No Pain No Gain </span>\n    </mat-toolbar>\n    <!-- Add Content Here -->\n    <router-outlet></router-outlet>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./src/app/pages/navbar/navbar.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/navbar/navbar.component.ts ***!
  \**************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.matches; }));
    }
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/pages/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/pages/navbar/navbar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/pages/registration/registration.component.css":
/*!***************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body { \n    background-color: lightgrey;\n}\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n\n.mt {\n    margin-top: 80px;\n}\n\nbutton {\n    margin-right: 50px;\n}\n\n.example-spacer {\n  flex: 1 1 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMkJBQTJCO0FBQy9COztBQUVBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtFQUNFLGNBQWM7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IHsgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tdCB7XG4gICAgbWFyZ2luLXRvcDogODBweDtcbn1cblxuYnV0dG9uIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDUwcHg7XG59XG5cbi5leGFtcGxlLXNwYWNlciB7XG4gIGZsZXg6IDEgMSBhdXRvO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/registration/registration.component.html":
/*!****************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <span>No Pain No Gain</span>\n  <span class=\"example-spacer\"></span>\n  <button [routerLink]=\"[ '/login' ]\" mat-button>Log in</button>\n</mat-toolbar>\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-2\"></div>\n    <div class=\"col-md-8 col-xs-12 col-sm-12 mt\">\n      <mat-card class=\"example-card\">\n        <mat-card-header>\n          <mat-card-title>Registrate!</mat-card-title>\n          <mat-card-subtitle>Registro de nuevos clientes!</mat-card-subtitle>\n        </mat-card-header>\n        <mat-card-content>\n          <form [formGroup]=\"registerForm\">\n            <div class=\"example-container\">\n              <mat-form-field>\n                <input formControlName=\"name\" matInput placeholder=\"Nombre Completo\" />\n                <mat-error *ngIf=\"G.name.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n              <mat-form-field>\n                <input formControlName=\"dni\" matInput placeholder=\"Cedula\" />\n                <mat-error *ngIf=\"G.dni.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n              <mat-form-field>\n                <input formControlName=\"email\" matInput placeholder=\"Correo Electronico\" />\n                <mat-error *ngIf=\"G.email.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n              <mat-form-field>\n                <mat-select formControlName=\"city\" (selectionChange)=\"getOffices($event.value)\" placeholder=\"Ciudades\">\n                  <mat-option *ngFor=\"let city of cities\" value=\"{{city._id}}\">{{city.name}}</mat-option>\n                </mat-select>\n                <mat-error *ngIf=\"G.city.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n              <mat-form-field>\n                <mat-select formControlName=\"boffice\" placeholder=\"Sedes\">\n                  <mat-option *ngFor=\"let office of offices\" value=\"{{office._id}}\">{{office.name}}</mat-option>\n                </mat-select>\n                <mat-error *ngIf=\"G.boffice.errors && submitted\">Campo requerido</mat-error>\n              </mat-form-field>\n            </div>\n          </form>\n        </mat-card-content>\n        <mat-card-actions>\n          <div class=\"container\" align=\"right\">\n            <button color=\"accent\" (click)=\"registerClient()\" mat-raised-button>Registrame</button>\n          </div>\n        </mat-card-actions>\n      </mat-card>\n    </div>\n    <div class=\"col-md-2\"></div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/registration/registration.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/registration/registration.component.ts ***!
  \**************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_boffice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/boffice.service */ "./src/app/services/boffice.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");





var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(offService, userService, builder) {
        var _this = this;
        this.offService = offService;
        this.userService = userService;
        this.builder = builder;
        this.submitted = false;
        this.getCities = function () {
            _this.offService.getCities().subscribe(function (data) {
                // tslint:disable-next-line:no-string-literal
                _this.cities = data['cities'];
            });
        };
        this.getOffices = function (id) {
            _this.offService.getOfficesbyId(id).subscribe(function (data) {
                // tslint:disable-next-line:no-string-literal
                _this.offices = data['branches'];
            });
            // console.log(id);
        };
        this.registerClient = function () {
            _this.submitted = true;
            if (_this.registerForm.invalid) {
                return;
            }
            _this.userService.postClient(_this.registerForm.value).subscribe(function (data) {
                alert(data['message']);
            });
        };
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        this.getCities();
        this.registerForm = this.builder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            dni: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            boffice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    Object.defineProperty(RegistrationComponent.prototype, "G", {
        get: function () { return this.registerForm.controls; },
        enumerable: true,
        configurable: true
    });
    RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.component.html */ "./src/app/pages/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.css */ "./src/app/pages/registration/registration.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_boffice_service__WEBPACK_IMPORTED_MODULE_2__["BofficeService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _localstorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./localstorage.service */ "./src/app/services/localstorage.service.ts");






var AuthService = /** @class */ (function () {
    function AuthService(http, store) {
        this.http = http;
        this.store = store;
        this.authenticateUrl = '/api/auth/admin/';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
    }
    AuthService.prototype.authenticate = function (value) {
        return this.http.post(this.authenticateUrl, value, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    AuthService.prototype.isAuth = function () {
        try {
            var token = this.store.getItem('auth');
            if (token !== null) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (e) {
            return false;
        }
    };
    AuthService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('Ha ocurrido un error: ', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code
            // The response body may contain clues as to what went wrong
            console.error("Backend returned error code " + error.status + " , " + ("body was: " + error.error));
        }
        // return an observable with user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _localstorage_service__WEBPACK_IMPORTED_MODULE_5__["LocalstorageService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/boffice.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/boffice.service.ts ***!
  \*********************************************/
/*! exports provided: BofficeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BofficeService", function() { return BofficeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _localstorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./localstorage.service */ "./src/app/services/localstorage.service.ts");






var BofficeService = /** @class */ (function () {
    function BofficeService(http, store) {
        this.http = http;
        this.store = store;
        this.CitiesUrl = '/api/city/';
        this.getOfficesbyIdUrl = '/api/branch/city/';
        this.BranchesUrl = '/api/branch/';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'authorization': this.store.getItem('auth')
            })
        };
    }
    BofficeService.prototype.getCities = function () {
        return this.http.get(this.CitiesUrl);
    };
    BofficeService.prototype.getOfficesbyId = function (id) {
        return this.http.get(this.getOfficesbyIdUrl + id);
    };
    BofficeService.prototype.getBranches = function () {
        return this.http.get(this.BranchesUrl);
    };
    BofficeService.prototype.postCity = function (value) {
        return this.http.post(this.CitiesUrl, value, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    BofficeService.prototype.postBranch = function (value) {
        return this.http.post(this.BranchesUrl, value, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    BofficeService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('Ha ocurrido un error: ', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code
            // The response body may contain clues as to what went wrong
            console.error("Backend returned error code " + error.status + " , " + ("body was: " + error.error));
        }
        // return an observable with user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    };
    BofficeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _localstorage_service__WEBPACK_IMPORTED_MODULE_5__["LocalstorageService"]])
    ], BofficeService);
    return BofficeService;
}());



/***/ }),

/***/ "./src/app/services/localstorage.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/localstorage.service.ts ***!
  \**************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setItem = function (key, value) {
        localStorage.setItem(key, value);
    };
    LocalstorageService.prototype.getItem = function (key) {
        return localStorage.getItem(key);
    };
    LocalstorageService.prototype.removeItem = function (key) {
        localStorage.removeItem(key);
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.ClientUrl = '/api/client/';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
    }
    UserService.prototype.postClient = function (value) {
        return this.http.post(this.ClientUrl, value, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    UserService.prototype.getClientbyOffice = function (officeId) {
        return this.http.get(this.ClientUrl + officeId);
    };
    UserService.prototype.getClients = function () {
        return this.http.get(this.ClientUrl);
    };
    UserService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('Ha ocurrido un error: ', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code
            // The response body may contain clues as to what went wrong
            console.error("Backend returned error code " + error.status + " , " + ("body was: " + error.error));
        }
        // return an observable with user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/asic/Documents/dev/mean-login/app/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map