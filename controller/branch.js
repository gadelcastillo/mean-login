'use strict'

const Branch = require('../model/branch');

function createBranch(req, res) {
    let branch = new Branch();

    branch.name = req.body.name;
    branch.city = req.body.city;
    branch.clients = 0;

    branch.save((err, branchSaved) => {
        if (err) {
            res.status(500).json({
                result: 'Error',
                message: 'Ha ocurrido un error al guardar en base de datos'
            });
        }
        res.status(200).json({
            result: 'Exito',
            message: 'Sucursal guardada correctamente',
            object: branchSaved
        });
    });
}

function getBranches(req, res) {
    Branch.find({}, (err, branches) => {
        if (err) {
            res.status(500).json({
                result: 'Error',
                message: 'Ha ocurrido un error al buscar en la base de datos'
            });
        }
        if (branches.lenght <= 0) {
            res.status(200).json({
                result: 'Precaucion',
                message: 'No existen sedes guardadas en base de datos'
            });
        }
        res.status(200).send({ branches })
    });
}

function getBranchesbyCity(req, res) {
  let cityId = req.params.cityId;

  Branch.find({ city : cityId }, (err, branches) => {
    if (err) {
      res.status(500).json({
        result: "Error",
        message: "Ha ocurrido un error al buscar en la base de datos"
      });
    }
    if (branches.lenght <= 0) {
      res.status(200).json({
        result: "Precaucion",
        message: "No existen sedes guardadas en base de datos"
      });
    }
    res.status(200).send({ branches });
  });
}

module.exports = {
  createBranch,
  getBranches,
  getBranchesbyCity
};