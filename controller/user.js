"use strict";

const User = require("../model/user");
const service = require("../service");

function createUser(req, res) {
  let user = new User();

  user.fullname = req.body.fullname;
  user.dni = req.body.dni;
  user.email = req.body.email;
  user.phone = req.body.phone;
  user.office = req.body.office;
  user.access = req.body.access;
  user.password = req.body.password;

  service.personExist(User, user.dni, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    try {
      if (!result) {
        user.save((error, userSaved) => {
          if (error) {
            console.log(error);
          } else {
            res.status(200).json({
              result: "Exito",
              message: "Usuario registrado correctamente"
            });
            res.end();
          }
        });
      } else {
        res.status(200).send({
          result: "Precaucion",
          message:
            "Su usuario ya se encuentra registrado! por favor haga login o recupere su contraseña"
        });
      }
    } catch (e) {
      console.log(e);
    }
  });
}

function createAdmin() {
  let user = new User();

  user.fullname = 'Admin';
  user.dni = 123456789;
  user.email = 'admin@gmail.com';
  user.phone = '369878878';
  user.office = 'Master';
  user.access = 'admin';
  user.password = 123123123;

  service.personExist(User, user.dni, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    try {
      if (!result) {
        user.save((error, userSaved) => {
          if (error) {
            console.log(error);
          } else {
            console.log("Usuario admin registrado correctamente")
          }
        });
      } else {
        console.log("Su usuario ya se encuentra registrado! por favor haga login o recupere su contraseña")
      }
    } catch (e) {
      console.log(e);
    }
  });
}

function getUsersbyBranch(req, res) {
    let branchId = req.params.boffice;

    User.find({ office: branchId }, (err, users) => {
        if (err) {
            res.status(500).json({
                result: 'Error',
                message: 'Ha ocurrido un error al buscar en la base de datos'
            });
        }
        if(!users) {
            res.status(200).json({
                result: 'Precaucion',
                message: 'No existen usuarios registrador en esa sede'
            });
        } else {
            res.status(200).send({ users });
        }
    });
}

module.exports = {
  createUser,
  getUsersbyBranch,
  createAdmin
};
