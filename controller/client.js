"use strict";

const Client = require("../model/client");
const Office = require("../model/branch");
const service = require("../service");

function registerClient(req, res) {
  let client = new Client();
  client.name = req.body.name;
  client.email = req.body.email; 
  client.dni = req.body.dni;
  client.city = req.body.city;
  client.boffice = req.body.boffice;

  service.personExist(Client, client.dni, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    try {
      if (!result) {
        Office.findById(client.boffice, "clients", (err, result) => {
          const numberClients = result.clients;
          if (numberClients < 300) {
            client.save((error, clientSaved) => {
              if (error) {
                console.log(error);
              } else {
                const newNum = 1 + numberClients;
                Office.findByIdAndUpdate(clientSaved.boffice,{ clients: newNum }, (err, result) => {
                  if (err) { console.log(err);}
                  console.log("updated");
                });
                res.status(200).json({
                  result: "Exito",
                  message:
                    "Bienvenido a No Pain No Gain, gracias por registrarse con nosotros"
                });
                res.end();
              }
            });
          } else {
            res.status(200).send({
              result: "Precaucion",
              message:
                "Esta sucursal ha alcanzado su limite, escoja otra por favor"
            });
          }
        });
      } else {
        res.status(200).send({
          result: "Precaucion",
          message:
            "Su usuario ya se encuentra registrado! por favor haga login o recupere su contraseña"
        });
      }
    } catch (e) {
      console.log(e);
    }
  });
}

function getClientsbyBranch(req, res) {
  let branchId = req.params.boffice;

  Client.find({ boffice: branchId }, (err, clients) => {
    if (err) {
      res.status(500).json({
        result: "Error",
        message: "Ha ocurrido un error al buscar en la base de datos"
      });
    }
    if (!clients) {
      res.status(200).json({
        result: "Precaucion",
        message: "No existen usuarios registrador en esa sede"
      });
    } else {
      res.status(200).send({ clients });
    }
  });
}

function getClients(req, res) {

  Client.find({  }, (err, clients) => {
    if (err) {
      res.status(500).json({
        result: "Error",
        message: "Ha ocurrido un error al buscar en la base de datos"
      });
    }
    if (!clients) {
      res.status(200).json({
        result: "Precaucion",
        message: "No existen usuarios registrador en esa sede"
      });
    } else {
      res.status(200).send({ clients });
    }
  });
}
module.exports = {
  registerClient,
  getClientsbyBranch,
  getClients
};
