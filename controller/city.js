'use strict'

const City = require('../model/city');

function createCity(req, res) {
    let city = new City();
    city.name = req.body.city_name;
    city.state = req.body.state;

    city.save((err, citySaved) => {
        if (err) {
            res.status(200).json({
                result: 'Error',
                message: 'Ha ocurrido un error al salvar la ciudad' 
            });
        }
        res.status(200).json({
            result: 'Exito',
            message: 'Se ha guardado correctamente',
            object: citySaved
        })
    });
}

function deleteCity(req, res) {
    let cityId = req.params.cityId;

    City.findById(cityId, (err, city) => {
        if (err) {
            res.status(500).json({
                result: 'Error',
                message: 'No se ha encontrado la ciudad en la base de datos'
            })
        }
        city.remove(err => {
            if (err) {
                res.status(500).json({
                    result: 'Error',
                    message: 'Ha ocurrido un error al borrar de la base de datos'
                });
            }
            res.status(200).json({
                result: 'Exito',
                message: 'Ciudad eliminada correctamente de la base de datos'
            });
        });
    });
}

function getCities(req, res) {
    City.find({}, (err, cities) => {
        if (err) {
            res.status(500).json({
                result: 'Error',
                message: 'Ha ocurrido un error al buscar en la base de datos'
            });
        }
        if(!cities) {
            res.status(200).json({
                result: 'Precaucion',
                message: 'No existen ciudades guardadas en base de datos'
            });
        }
        res.status(200).send({ cities })
    });
}

module.exports = {
    createCity,
    getCities,
    deleteCity                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
}