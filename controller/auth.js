"use strict";

const User = require("../model/user");
const service = require("../service");

function signIn(req, res) {
  let username = req.body.username;
  let password = req.body.password;

  User.find({ email: username }, (err, user) => {
    if (err) {
      throw err;
    }
    if (!user) {
      res.status(200).json({
        result: "Error",
        message: "Usuario incorrecto"
      });
    } else {
      let payload = service.openToken(user[0].password);
      if (payload.pass == password) {
        res.status(200).json({
          result: "Exito",
          message: "Autenticacion exitosa",
          token: service.createToken(user),
          redirectTo: '/dashboard'
        });
      } else {
        res.status(200).json({
          result: "Error",
          message: "Contraseña invalida"
        });
      }
    }
  });
}

/* function signOut(req, res) {

} */

module.exports = {
  signIn
};
