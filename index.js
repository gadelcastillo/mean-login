'use strict'

const app = require('./app');
const config = require('./config');
const mongoose = require('mongoose');
const admin = require('./controller/user');

mongoose.connect(config.MONGO , {useNewUrlParser: true}, (err, rsp) => {
    if(err){
        return console.log(`${err}`);
    } else {
        console.log('Connection stablished...');
        app.listen(config.PORT, function () { console.log(`Server working on port ${config.PORT}`); });
        admin.createAdmin();
    }
})