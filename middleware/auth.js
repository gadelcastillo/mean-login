"use strict";

const config = require("../config");
const jwt = require("jwt-simple");
const moment = require("moment");

function isAuth(req, res, next) {
  if (!req.headers.authorization) {
    console.log("403: Acceso denegado");
    return res.status(200).json({ result: "Denied", message: "Acceso denegado" });
  }

  try {
    const token = req.headers.authorization;
    const payload = jwt.decode(token, config.SECRET_TOKEN);
      if (payload.exp <= moment.unix()) {
        return res.status(200).json({ result: 'Denied', message: "Token ha expirado" });
      }
      if (payload.number > 1) {
        console.log("403: Acceso denegado");
        return res.status(200).json({ result: "Denied", message: "Acceso denegado, usuario invalido" });
      }
      req.user = payload.sub;
      next();
  } catch (er) {
    console.log(er);
    res
      .status(200)
      .json({
        result: "Denied",
        message: "Acceso denegado, usuario invalido"
      });
  }


}

module.exports = { isAuth };
