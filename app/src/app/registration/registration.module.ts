import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './components/registration/registration.component';

@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    CommonModule
  ]
})
export class RegistrationModule { }
