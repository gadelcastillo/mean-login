export interface Office {
    _id: string;
    name: string;
    city: string;
    clients: number,
    _v: string;
}
