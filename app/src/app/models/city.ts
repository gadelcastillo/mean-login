export interface City {
    _id: number;
    name: string;
    state: string;
    _v: number;
}
