import { Component, OnInit } from '@angular/core';
import { BofficeService } from '../../services/boffice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public cities;
  public offices;
  public registerForm: FormGroup;
  public submitted = false;

  constructor(
    private offService: BofficeService,
    private userService: UserService,
    private builder: FormBuilder
  ) { }

  ngOnInit() {
    this.getCities();

    this.registerForm = this.builder.group({
      name: ['', Validators.required],
      dni: ['', Validators.required],
      email: ['', Validators.required],
      city: ['', Validators.required],
      boffice: ['', Validators.required]
    });
  }

  getCities = () => {
    this.offService.getCities().subscribe(
      (data) => {
        // tslint:disable-next-line:no-string-literal
        this.cities = data['cities'];
      }
    );
  }

  getOffices = (id) => {
    this.offService.getOfficesbyId(id).subscribe(
      (data) => {
        // tslint:disable-next-line:no-string-literal
        this.offices = data['branches'];
      }
    );
    // console.log(id);
  }

  registerClient = () => {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.userService.postClient(this.registerForm.value).subscribe(
      (data) => {
        alert(data['message']);
      }
    );
  }

  get G() { return this.registerForm.controls; }

}
