import { Component, OnInit, ViewChild } from '@angular/core';
import { BofficeService } from '../../services/boffice.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  branches: Array<any>;
  displayedColumns: string[] = ['numero', 'nombre', 'dni', 'correo', 'registro'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private boffice: BofficeService,
    private clientService: UserService
  ) { }

  ngOnInit() {
    this.getBranches();
  }

  getBranches = () => {
    this.boffice.getBranches().subscribe(
      data => {
        // console.log(data['branches']);
        this.branches = data['branches'];
      }
    );
  }

  getClients = () => {
    this.clientService.getClients().subscribe(
      data => {
        console.log(data['clients']);
        this.dataSource = new MatTableDataSource(data['clients']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  getClientsbyOffice = (id) => {
    if (id === 'all') {
      this.getClients();
    } else {
      this.clientService.getClientbyOffice(id).subscribe(
        (data) => {
          console.log(data['clients']);
          this.dataSource = new MatTableDataSource(data['clients']);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      );
    }
  }

}
