import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BofficeService } from '../../services/boffice.service';
import { City } from '../../models/city';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  displayedColumns: string[] = ['numero', 'ciudad', 'estado'];
  dataSource: MatTableDataSource<City>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dialogRef;
  createForm: FormGroup;
  submitted = false;

  constructor(
    private boffice: BofficeService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private formbuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getCities();

    this.createForm = this.formbuilder.group({
      city_name: ['', Validators.required],
      state: ['', Validators.required]
    });
  }

  getCities = () => {
    this.boffice.getCities().subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource(data['cities']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  openDialog(templateId): void {
    this.dialogRef = this.dialog.open(templateId, {
      width: '60%',
    });
  }

  submitForm = () => {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    this.boffice.postCity(this.createForm.value).subscribe(
      (response) => {
        if (response['result'] !== 'Error'){
          this.snackbar.open(response['message'], response['result'], { duration: 3000 });
          this.dialogRef.close();
          this.createForm.reset();
          this.getCities();
        }
      }
    );
  }

  get G() { return this.createForm.controls; }

}
