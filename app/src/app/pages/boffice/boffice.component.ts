import { Component, OnInit, ViewChild } from '@angular/core';
import { BofficeService } from '../../services/boffice.service';
import { City } from '../../models/city';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-boffice',
  templateUrl: './boffice.component.html',
  styleUrls: ['./boffice.component.css']
})
export class BofficeComponent implements OnInit {

  cities: Array<City>;
  displayedColumns: string[] = ['numero', 'nombre', 'clientes', 'registro'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dialogRef;
  createForm: FormGroup;
  submitted = false;

  constructor(
    private boffice: BofficeService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private formbuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getBranches();
    this.getCities();
    this.createForm = this.formbuilder.group({
      name: ['', Validators.required],
      city: ['', Validators.required]
    });
  }

  get G() { return this.createForm.controls; }

  openDialog(templateId): void {
    this.dialogRef = this.dialog.open(templateId, {
      width: '60%',
    });
  }

  getBranches = () => {
    this.boffice.getBranches().subscribe(
      data => {
        // console.log(data['branches']);
        this.dataSource = new MatTableDataSource(data['branches']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  getOfficesbyId = (id) => {
    if (id === 'all') {
      this.getBranches();
    } else {
      this.boffice.getOfficesbyId(id).subscribe(
        (data) => {
          this.dataSource = new MatTableDataSource(data['branches']);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      );
    }
  }

  getCities = () => {
    this.boffice.getCities().subscribe(
      data => {
        this.cities = data['cities'];
      }
    )
  }

  submitForm = () => {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    this.boffice.postBranch(this.createForm.value).subscribe(
      response => {
        if (response['result'] !== 'Error') {
          this.snackbar.open(response['message'], response['result'], { duration: 3000 });
          this.dialogRef.close();
          this.createForm.reset();
          this.getBranches();
        }
      }
    );
  }

}
