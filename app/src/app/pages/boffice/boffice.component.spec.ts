import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BofficeComponent } from './boffice.component';

describe('BofficeComponent', () => {
  let component: BofficeComponent;
  let fixture: ComponentFixture<BofficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BofficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BofficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
