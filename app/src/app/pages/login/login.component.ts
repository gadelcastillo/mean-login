import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { LocalstorageService } from '../../services/localstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted = false;

  constructor(
    private builder: FormBuilder,
    private auth: AuthService,
    private snackbar: MatSnackBar,
    private router: Router,
    private store: LocalstorageService
  ) { }

  ngOnInit() {
    this.loginForm = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get G() { return this.loginForm.controls; }

  signIn = () => {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    console.log(this.loginForm.value);
    this.auth.authenticate(this.loginForm.value).subscribe(
      (data) => {
        this.store.setItem('auth', data['token']);
        this.snackbar.open(data['message'] , data['result'], { duration: 3000})
          .afterDismissed().subscribe(
            (action) => {
              this.router.navigate([data['redirectTo']]);
            }
          );
      }
    );
  }

}
