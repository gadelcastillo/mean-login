import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authenticateUrl = '/api/auth/admin/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient,
    private store: LocalstorageService,
  ) { }

  public authenticate(value) {
    return this.http.post(this.authenticateUrl, value, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  public isAuth(): boolean {
    try {
      const token = this.store.getItem('auth');
      if (token !== null) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Ha ocurrido un error: ', error.error.message);
    } else {
      // The backend returned an unsuccessful response code
      // The response body may contain clues as to what went wrong
      console.error(
        `Backend returned error code ${error.status} , ` + `body was: ${error.error}`);
    }
    // return an observable with user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
