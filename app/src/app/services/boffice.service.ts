import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalstorageService } from './localstorage.service';
import { City } from '../models/city';
import { Office } from '../models/office';

@Injectable({
  providedIn: 'root'
})
export class BofficeService {

  private CitiesUrl = '/api/city/';
  private getOfficesbyIdUrl = '/api/branch/city/';
  private BranchesUrl = '/api/branch/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': this.store.getItem('auth')
    })
  };
  constructor(
    private http: HttpClient,
    private store: LocalstorageService
  ) { }

  public getCities() {
    return this.http.get<City[]>(this.CitiesUrl);
  }

  public getOfficesbyId(id) {
    return this.http.get<Office[]>(this.getOfficesbyIdUrl + id);
  }

  public getBranches() {
    return this.http.get(this.BranchesUrl);
  }

  public postCity(value) {
    return this.http.post<City>(this.CitiesUrl, value, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  public postBranch(value) {
    return this.http.post(this.BranchesUrl, value, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Ha ocurrido un error: ', error.error.message);
    } else {
      // The backend returned an unsuccessful response code
      // The response body may contain clues as to what went wrong
      console.error(
        `Backend returned error code ${error.status} , ` + `body was: ${error.error}`);
    }
    // return an observable with user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
