import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private ClientUrl = '/api/client/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  public postClient(value) {
    return this.http.post(this.ClientUrl, value, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  public getClientbyOffice(officeId) {
    return this.http.get(this.ClientUrl + officeId);
  }

  public getClients() {
    return this.http.get(this.ClientUrl);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Ha ocurrido un error: ', error.error.message);
    } else {
      // The backend returned an unsuccessful response code
      // The response body may contain clues as to what went wrong
      console.error(
        `Backend returned error code ${error.status} , ` + `body was: ${error.error}`);
    }
    // return an observable with user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
