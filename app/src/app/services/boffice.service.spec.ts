import { TestBed } from '@angular/core/testing';

import { BofficeService } from './boffice.service';

describe('BofficeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BofficeService = TestBed.get(BofficeService);
    expect(service).toBeTruthy();
  });
});
