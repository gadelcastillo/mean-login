import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './pages/registration/registration.component';
import { LoginComponent } from './pages/login/login.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { ClientComponent } from './pages/client/client.component';
import { CityComponent } from './pages/city/city.component';
import { BofficeComponent } from './pages/boffice/boffice.component';

import { IsloggedGuard } from './guards/islogged.guard';

const routes: Routes = [
  { path: '', redirectTo: 'register', pathMatch: 'full' },
  { path: 'register' , component: RegistrationComponent },
  { path: 'login' , component: LoginComponent },
  { path: 'dashboard' , component: NavbarComponent , canActivate: [IsloggedGuard] ,
    children : [
      { path: 'cities', component: CityComponent },
      { path: 'branches', component: BofficeComponent },
      { path: 'clients', component: ClientComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
