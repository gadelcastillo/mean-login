"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const jwt = require('jwt-simple');
const config = require('../config');

const UserSchema = Schema({
  fullname: String,
  dni: { type: Number, unique: true },
  email: { type: String, unique: true, lowercase: true },
  phone: Number,
  office: String,
  access: { type: String, enum: ["admin", "client"] },
  password: { type: String, select: true },
  registered: { type: Date, default: Date.now() }
});

UserSchema.pre("save", function(next) {
  let user = this;

  let payload = {
    sub: user.dni,
    pass: user.password
  };

  user.password = jwt.encode(payload, config.SECRET_TOKEN);
  next();

});

module.exports = mongoose.model("Users", UserSchema);
