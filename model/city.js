'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CitySchema = Schema({
    name: String,
    state: String,
});

module.exports = mongoose.model('Cities', CitySchema);