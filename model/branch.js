'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BranchSchema = Schema({
    name: String,
    city: String,
    clients: Number,
    registered: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Branchs', BranchSchema);