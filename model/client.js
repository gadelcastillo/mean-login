'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientSchema = Schema({
    name: String,
    dni: { type: Number, unique: true},
    email: String,
    city: String,
    boffice: String,
    registered: { type: Date, default: Date.now()}
});

module.exports = mongoose.model('Clients', ClientSchema);