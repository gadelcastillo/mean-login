"use strict";

const jwt = require("jwt-simple");
const moment = require("moment");
const config = require("../config");
const User = require("../model/user");
const Client = require("../model/client");
const bcrypt = require("bcrypt-nodejs");

function createToken(user) {
  const payload = {
    sub: user.dni,
    name: user.fullname,
    email: user.email,
    number: user.access,
    iat: moment.unix(),
    exp: moment()
      .add(14, "hours")
      .unix()
  };

  return jwt.encode(payload, config.SECRET_TOKEN);
}

function personExist(Model ,dni , callback) {

  Model.find({ dni: dni }, (err, result) => {
    if (err) {
      const err = {
        result: "Error",
        message: `Ha ocurrido un error con la base de datos: ${err}`
      };
      callback(err, null);
    }
    if (result.length > 0) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  });
}

function openToken (token) {
  return jwt.decode(token, config.SECRET_TOKEN);
}

module.exports = {
  createToken,
  openToken,
  personExist
};
