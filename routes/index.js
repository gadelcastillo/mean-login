'use strict'

const express = require('express');
const path = require('path');

// Middleware
const auth = require('../middleware/auth');

// Controllers 
const city = require('../controller/city');
const branch = require('../controller/branch');
const user = require('../controller/user');
const client = require('../controller/client');
const authController = require('../controller/auth');

const api = express.Router();

api.get('/', (req, res) => { res.sendFile(path.join(__dirname)) });

// Leer todas las ciudades 
api.get('/api/city/', city.getCities);
// Borrar una ciudad utilizando su id
api.delete("/api/city/:cityId", auth.isAuth, city.deleteCity);
// Crear una nueva ciudad en base de datos
api.post('/api/city/', auth.isAuth , city.createCity);

// Crear una nueva sede en base de datos
api.post("/api/branch/", auth.isAuth, branch.createBranch);
// Leer todas las sedes guardadas en base de datos
api.get('/api/branch/', branch.getBranches);
// Leer las sedes filtrando por ciudad
api.get('/api/branch/city/:cityId', branch.getBranchesbyCity);

// Crear un nuevo usuario administrador
api.post('/api/register/admin/', auth.isAuth ,user.createUser);
// Iniciar sesion administrador
api.post("/api/auth/admin/", authController.signIn);

// Registrar un cliente nuevo 
api.post('/api/client/', client.registerClient);
// Visualizar clientes registrados por sucursal 
api.get('/api/client/:boffice', client.getClientsbyBranch);
// Ver todos los clientes
api.get("/api/client/", client.getClients);

module.exports = api; 